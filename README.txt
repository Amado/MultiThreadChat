This app is a simple TUI(Text User Interfce) room chat for local networks.
The project is an approach about how to use multithread sockets connection in java, 
additionally the program integrate db4o persistence for the clients nicks. 
It's especially interesting about how to manage the broadcasting from the local server 
to all clients using synchronized clients threads.

In the docs folder, you can find the javadoc.

Technologies :

	db4o
	Sockets
	Threads