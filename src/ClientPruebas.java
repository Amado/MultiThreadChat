import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Scanner;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.cs.Db4oClientServer;
import com.db4o.query.Predicate;


/**
 * 
 * @author Emilio Amado <emiliojoseav@gmail.com>
 * 
 * Cliente de chat.
 * 
 * This is free software, licensed under the GNU General Public License
 * v3. See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class ClientPruebas {
	// for I/O
	private ObjectInputStream sInput;
	private static ObjectOutputStream sOutput;
	private Socket socket;

	private String server, username;
	private int port;

	/**
	 * CONSTRUCOR.
	 * 
	 * @param server server addr
	 * @param port puerto del server
	 * @param username nombre del usuario
	 */
	ClientPruebas(String server, int port, String username) {
		this.server = server;
		this.port = port;
		this.username = username;
	}

	/**
	 * Valida los args al arancar el cliente e inicializa los servicios.
	 * 
	 * @param args los argumentos al ejecutar el client.jar
	 */
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		//valores por defecto si no se introducen args
		int portNumber = 1500;
		String serverAddress = "localhost";
		String userName = "Anónimo";

		//se evalúan los argumentos
		switch(args.length) {
			case 3:
				serverAddress = args[2];
				userName = args[0];
				try {
					portNumber = Integer.parseInt(args[1]);
				}
				catch(Exception e) {
					System.out.println("Valor de puerto numérico.");
					System.out.println("Uso: > java -jar Client.jar [username] [portNumber] [serverAddress]");
					return;
				}
			case 2:
				try {
					portNumber = Integer.parseInt(args[1]);
				}
				catch(Exception e) {
					System.out.println("Valor de puerto numérico.");
					System.out.println("Uso: > java -jar Client.jar [username] [portNumber]");
					return;
				}
			case 1: 
				userName = args[0];
			case 0:
				break;
			default:
				System.out.println("Usage is: > java -jar Client.jar [username] [portNumber] [serverAddress]");
			return;
		}
		//se crea un nuevo client para poder usar sus métodos
		ClientPruebas client = new ClientPruebas(serverAddress, portNumber,userName);
		
		//valida el nick de usuario, si no es válido sale de la app.
		if(!client.validaNick(userName)){
			System.out.println("\nNick no disponible, por favor selecciona otro.\n");
			return;
		}
		
		//inicializamos los servicios para el chat, si no se pueden inicializar
		//salimos del programa
		if (!client.startChat())
			return;

		
		//bucle que lee constantemente los mensajes del usuario del cliente
		// hacia el server. client -> server
		while (true) {
			try {
				System.out.print("> ");
				// lee el mensaje del usuario
				String msg = scan.nextLine();
				
				if (msg.equalsIgnoreCase("LOGOUT")) {
					sOutput.writeObject("LOGOUT");
					break;
				}
				else if (msg.equalsIgnoreCase("WHOISIN")) {
					sOutput.writeObject("WHOISIN");
				} else {
					sOutput.writeObject(msg);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		client.disconnect();
	}

	/**
	 * Inicializa el socket del cliente para conectar con el server,
	 * además de inicializar los streams para enviar o recivir los mensajes.
	 * 
	 * @return true si todo se ha inicializado
	 */
	public boolean startChat() {
		//nos conectamos al servidor e inicializamos los streams
		try {
			socket = new Socket(server, port);

		String msg = "Conexión confirmada " + socket.getInetAddress() + ":"+ socket.getPort();
		System.out.println(msg);

			sInput = new ObjectInputStream(socket.getInputStream());
			sOutput = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException eIO) {
			System.out.println("Error de conexión con el servidor");
			return false;
		}

		// inicializamos en un segunda thread la escucha del cliente con el
		//servidor. server -> client
		new ListenFromServer().start();
		
		//enviamos al server nuestro nombre de usuario.
		try {
			sOutput.writeObject(username);
		} catch (IOException eIO) {
			disconnect();
			return false;
		}
		return true;
	}


	/**
	 * Desconecta los streams y el servidor.
	 */
	private void disconnect() {
			try {
				if (sInput != null)
					sInput.close();
				if (sOutput != null)
					sOutput.close();
				if (socket != null)
					socket.close();
			} catch (Exception e) {
		}
	}
	
	
	/**
	 * Valida el nick de usuario introducido como arg al iniciar la app
	 * desde la consola.
	 * 
	 * @param nick el nick a validar
	 * @return true si el nick no está en uso o si no es "Anónimo"
	 */
	private static boolean validaNick(String nick){
		ObjectContainer db4oClient = null;
		ClientNick nickObject= new ClientNick(nick);
		
		try{
			db4oClient = Db4oClientServer.openClient("127.0.0.1", 8732, "user", "1234");
		}
		catch(Exception e){
			System.out.println("Nicks DB no disponible.");
		}
		
		ObjectSet<ClientNick> result = db4oClient.query(new Predicate<ClientNick>() {
			@Override
			public boolean match(ClientNick nick) {
				return true;
			}
		});
		while (result.hasNext()) {
			ClientNick cNick = result.next();
			if( (cNick.getNick().equals(nick)) && (!cNick.getNick().equals("Anónimo"))){
				return false;
			}
		}
		db4oClient.store(nickObject);
		db4oClient.commit();
		return true;
	}


	/**
	 * 
	 * @author Emilio Amado <emiliojoseav@gmail.com>
	 * 
	 * Escucha constantemente los mensajes del servidor, en un segundo thread
	 * para poder imprimirlas por pantalla.
	 * 
	 * This is free software, licensed under the GNU General Public License
	 * v3. See http://www.gnu.org/licenses/gpl.html for more information.
	 */
	class ListenFromServer extends Thread {

		public void run() {
			while (true) {
				try {
					String msg = (String) sInput.readObject();
					System.out.println(msg);
					System.out.print("> ");

				} catch (IOException e) {
					System.out.println("Servidor offline: ");
					break;
				}
				catch (ClassNotFoundException e2) {
				}
			}
		}
	}
}