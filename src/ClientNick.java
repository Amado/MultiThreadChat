/**
 * 
 * @author Emilio Amado <emiliojoseav@gmail.com>
 * 
 *         Modela el nick de un usuario.
 * 
 *         This is free software, licensed under the GNU General Public License
 *         v3. See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class ClientNick {

	private String nick;

	public ClientNick(String nick) {
		super();
		this.nick = nick;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	@Override
	public String toString() {
		return "ClientNick [nick=" + nick + "]";
	}

}
