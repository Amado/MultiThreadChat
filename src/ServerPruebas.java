import java.io.*;
import java.net.*;
import java.util.*;

import com.db4o.ObjectContainer;
import com.db4o.ObjectServer;
import com.db4o.ObjectSet;
import com.db4o.cs.Db4oClientServer;
import com.db4o.query.Predicate;

/**
 * 
 * @author Emilio Amado <emiliojoseav@gmail.com>
 * 
 * Servidor de chat, centraliza todas las conexiónes de los clientes 
 * y gestiona los mensajes para distribuirlos a todos los clientes.
 * 
 * This is free software, licensed under the GNU General Public License
 * v3. See http://www.gnu.org/licenses/gpl.html for more information.
 */
public class ServerPruebas {
	
	//almacena los clientes conectados
	private ArrayList<ClientThread> arrayClients = new ArrayList<ClientThread>();
	//puerto de conexión
	private int port;
	//para mantener el servidor abierto
	private boolean keepGoing;
	//socket del servidor
	private static ServerSocket serverSocket = null;

	
	/**
	 * CONSTRUCTOR.
	 * 
	 * @param port el puerto de conexión
	 */
	public ServerPruebas(int port) {
		this.port = port;
	}

	/**
	 * Valida los args al arrancar el cliente e inicializa los servicios.
	 * 
	 * @param args los argumentos al ejecutar el client.jar
	 */
	public static void main(String[] args) {
		int portNumber = 1500;

		switch(args.length) {
			case 1:
				try {
					portNumber = Integer.parseInt(args[0]);
				}
				catch(Exception e) {
					System.out.println("Valor de puerto numérico.");
					System.out.println("Uso: > java -jar Server.jar [portNumber]");
					return;
				}
			case 0:
				break;
			default:
				System.out.println("Uso: > java -jar Server.jar [portNumber]");
				return;
		}
		
		Db4oNickServer nickDB = new Db4oNickServer();
		Thread nickDBThread = new Thread(nickDB,"nickDbThread");
		nickDBThread.start();
		
		// crea un nuevo server para poder usar sus métodos.
		ServerPruebas server = new ServerPruebas(portNumber);
		server.startChat();
	}

	//inicializamos los servicios para el chat
	public void startChat() {
		keepGoing = true;
		try {
			// socket del servidor
			serverSocket = new ServerSocket(port);

			// bucle que permanentemente espara para nuevas conexiones
			while (keepGoing) {

				System.out.println("Esperando clientes ...");
				Socket socket = serverSocket.accept(); 
				System.out.println("Cliente conectado.");
				
				//iniciamos un nuevo thread para cada cliente
				ClientThread clientThread = new ClientThread(socket);
				clientThread.start();
				
				//almacenamos el cliente en la lista de clientes
				arrayClients.add(clientThread);

			}
			//si no se cierra el bucle ya no se esperan nuevas conexiones
			//y cerramos los streams, el socket de servidor y todos los clientes.
			try {
				serverSocket.close();
				for (int i = 0; i < arrayClients.size(); ++i) {
					ClientThread clientThread = arrayClients.get(i);
					clientThread.sInput.close();
					clientThread.sOutput.close();
					clientThread.socket.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Envía el mensaje de algún cliente a todos los clientes
	 * almacenados en el arrayClientes.
	 * 
	 * @param message el mensaje a enviar
	 */
	private synchronized void broadcast(String message) {

		String messageLf = message + "\n";
		//muestra el mensaje al servidor
		//así el server puede monitorear los mensajes de los clientes
		System.out.print(messageLf);

		//enviamos el mensaje a todos los clientes, si el envío falla
		//el cliente es borrado de arrayClientes.
		for (int i = 0;i<arrayClients.size();i++) {
			ClientThread clientThread = arrayClients.get(i);
			
			//se inetnta enviar el mensaje
			if (!clientThread.writeMsg(messageLf)) {
				arrayClients.remove(i);
				System.out.println("Cliente " + clientThread.username+ " desconectado y borrado de la lista.");
			}
		}
	}

	
	/**
	 * 
	 * @author Emilio Amado <emiliojoseav@gmail.com>
	 * 
	 * Por cada cliente conectado creamos una nueva instancia de esta 
	 * clase para que los mensajes de entrada y salida puedan correr
	 * en un segundo thread.
	 * 
	 * This is free software, licensed under the GNU General Public License
	 * v3. See http://www.gnu.org/licenses/gpl.html for more information.
	 */
	class ClientThread extends Thread {

		Socket socket;
		ObjectInputStream sInput;
		ObjectOutputStream sOutput;
		String username;
		String msg;


		/**
		 * Constructor, se inicializan los streams y el socket de cliente.
		 * @param socket
		 */
		ClientThread(Socket socket) {
			this.socket = socket;
			try {
				sOutput = new ObjectOutputStream(socket.getOutputStream());
				sInput = new ObjectInputStream(socket.getInputStream());
				username = (String) sInput.readObject();
			} catch (IOException e) {
				//si no se pueden iniciar los streams salimos del construcotr.
				e.printStackTrace();
				return;
			}
			catch (ClassNotFoundException e) {
			}
		}

		//bucle que permaentemente para que el server escuche los mensajes de los clientes.
		// client -> server
		public void run() {
			// to loop until LOGOUT
			boolean keepGoing = true;
			while (keepGoing) {
				
				try {
					msg = (String) sInput.readObject();
					// the messaage part of the ChatMessage
					String message = msg;
	
					//se evalúa el mensaje recibido
					switch (msg) {
					//el cliente sale del chat
					case "LOGOUT":
						System.out.println(username + " se ha desconectado.");
						keepGoing = false;
						break;
					//se hace una lista de todos los clientes conectados
					case "WHOISIN":
						writeMsg("Lista de los clientes conectados: \n");
						for (int i = 0; i < arrayClients.size(); ++i) {
							ClientThread ct = arrayClients.get(i);
							writeMsg((i + 1) + ") " + ct.username);
						}
						break;
					//mensaje ordinario
					default:
						//se envía el mensaje a todos los clientes conectados.
						broadcast(username + ": " + message);
						break;
				}
				} catch (IOException e) {
					System.out.println(username+ ", no se ha podido leer el mensaje: " + e);
					break;
				} catch (ClassNotFoundException e2) {
					break;
				}
			}
			close();
		}

		/**
		 * Cierra los streams y el socket de cliente.
		 */
		private void close() {
			// try to close the connection
			try {
				sOutput.close();
				sInput.close();
				socket.close();
			} catch (Exception e) {
			}
		}


		/**
		 * Intenta enviar un mensaje, si es que el socket no ha sido cerrado.
		 * 
		 * @param msg el mensaje a enviar
		 * @return si el mensaje ha podido salir
		 */
		private boolean writeMsg(String msg) {
			//si falla sale con false
			if (!socket.isConnected()) {
				close();
				return false;
			}
			try {
				sOutput.writeObject(msg);
			}
			catch (IOException e) {
				System.out.println("No se ha podido mandar el mensaje a " + username);
			}
			return true;
		}
	}
	
	
	/**
	 * 
	 * @author Emilio Amado <emiliojoseav@gmail.com>
	 * 
	 * Crea un db4o DB para almacenar los nicks de los usuarios.
	 * 
	 * This is free software, licensed under the GNU General Public License
	 * v3. See http://www.gnu.org/licenses/gpl.html for more information.
	 */
	public static class Db4oNickServer implements Runnable{
		
		private boolean stopDb4oServer = false;
		private ObjectContainer db4oClient = null;
		Scanner sc = new Scanner(System.in);

		@Override
		public void run() {
			synchronized (this) {
				ObjectServer server = Db4oClientServer.openServer(
						Db4oClientServer.newServerConfiguration(),
						"d4oNickDB.yap", // el nombre del fichero del servidor
						8732); // el puerto para el servidor

				Thread.currentThread().setName(this.getClass().getName());

				// creamos usuarios y damos permisos de acceso
				server.grantAccess("user", // el nombre del usuario
								   "1234");// el password para el usuario
				try {
					while (!stopDb4oServer) {
						String comandoServer = sc.nextLine();
						if(comandoServer.equals("STOP")){
							removeDb();
						}
						else if(comandoServer.equals("ALLNICKS")){
							printAllNicks();
						}
					}
					server.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					server.close();
				}
			}
		}

		public void removeDb(){
			db4oClient = Db4oClientServer.openClient("127.0.0.1", 8732, "user", "1234");
			// almacenamos las usuarios en un Objectset
			ObjectSet<ClientNick> result = db4oClient.query(new Predicate<ClientNick>() {
				@Override
				public boolean match(ClientNick nick) {
					return true;
				}
			});
			while (result.hasNext()) {
				db4oClient.delete(result.next());
				db4oClient.commit();
			}
			db4oClient.close();
			System.exit(0);
		}
		
		public void printAllNicks(){
			db4oClient = Db4oClientServer.openClient("127.0.0.1", 8732, "user", "1234");
			// almacenamos las usuarios en un Objectset
			ObjectSet<ClientNick> result = db4oClient.query(new Predicate<ClientNick>() {
				@Override
				public boolean match(ClientNick nick) {
					return true;
				}
			});
			
			if(!result.hasNext()){
				System.out.println("No hay nicks registrados");
			}
			
			while (result.hasNext()) {
				System.out.println(result.next().toString());
			}
		}
	}
	
}



